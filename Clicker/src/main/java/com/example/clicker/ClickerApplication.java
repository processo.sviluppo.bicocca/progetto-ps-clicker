package com.example.clicker;
////ClickerApplication
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//
//@SpringBootApplication
//public class ClickerApplication {
//
//	public static void main(String[] args) {
//		SpringApplication.run(ClickerApplication.class, args);
//	}
//	
//	
//
//}

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
@SpringBootApplication
public class ClickerApplication extends SpringBootServletInitializer {
 public static void main(String[] args) {
  SpringApplication.run(ClickerApplication.class, args);
 }
 
 @Override
 protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
  return application.sources(ClickerApplication.class);
 }
}