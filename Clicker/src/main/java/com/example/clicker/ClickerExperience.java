package com.example.clicker;

import com.example.clicker.controllers.BuildingController;
import com.example.clicker.controllers.LevelController;
import com.example.clicker.controllers.SpecialBuildingController;
import com.example.clicker.entities.Building;
import com.example.clicker.entities.Level;
import com.example.clicker.entities.SpecialBuilding;
import com.example.clicker.repositories.UserBuildingRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.clicker.controllers.UserController;

@Component
public class ClickerExperience implements CommandLineRunner {

	private static final Logger logger = LogManager.getLogger(UserController.class);

	@Autowired
    private UserController userController;
	@Autowired
	private BuildingController buildingController;
	@Autowired
	private SpecialBuildingController specialBuildingController;
	@Autowired
	private LevelController levelController;
	@Autowired
	private UserBuildingRepository userBuildingRepository;

	@Override
	public void run(String... args) throws Exception {

		logger.info("Cleaning up the database.");
		userBuildingRepository.deleteAll();
		userController.deleteAll();
		buildingController.deleteAll();
		specialBuildingController.deleteAll();
		levelController.deleteAll();


		logger.info("Start Click-game experience.");

		logger.info("Add the first user Lucrezia.");

		this.userController.signUp("Lucrezia");

		logger.info("Add the second user Achille.");

		userController.signUp("Achille");

		logger.info("Set Achille as best friend of Lucrezia.");

		userController.becomeBF("Lucrezia","Achille");

		logger.info("Lucrezia is clicking 5 times and gains 25 gems.");
		logger.info("Lucrezia is clicking 5 times and gains 50 money.");

		for(int i=0;i<5;i++) {
			userController.generateGems("Lucrezia");
			userController.generateMoney("Lucrezia");
		}

		logger.info("Lucrezia has " + userController.getGems("Lucrezia") + " gems and " + userController.getMoney("Lucrezia") +
				" money.");

		logger.info("Achille has " + userController.getGems("Achille") + " gems and " + userController.getMoney("Achille") +
				" money.");

		logger.info("Lucrezia donates 30 money and 15 gems to Achille.");
		for(int i=0;i<3;i++){
			userController.donateGems("Lucrezia", "Achille");
			userController.donateMoney("Lucrezia", "Achille");
		}

		logger.info("Lucrezia has " + userController.getGems("Lucrezia") + " gems and " + userController.getMoney("Lucrezia") +
				" money.");

		logger.info("Achille has " + userController.getGems("Achille") + " gems and " + userController.getMoney("Achille") +
				" money.");

		logger.info("Insert of building Torre");
		Building torre =  new Building();
		torre.setName("Torre");
		torre.setDescription("Torre base");
		torre.setArea(1000);
		torre.setPeople(5);
		torre.setFloors(2);
		buildingController.insertBuilding(torre);

		logger.info("Insert of special building Castello");
		SpecialBuilding castello = new SpecialBuilding();
		castello.setName("Castello");
		castello.setDescription("Castello base");
		castello.setArea(5000);
		castello.setPeople(100);
		castello.setFloors(6);
		castello.setCongressRooms(4);
		specialBuildingController.insertBuilding(castello);

		logger.info("Insert of levels 1 and 2");
		Level level1 = new Level();
		level1.setId(1);
		level1.setGemsCost(0);
		level1.setMoneyCost(0);
		levelController.insertLevel(level1);
		Level level2 = new Level();
		level2.setId(2);
		level2.setGemsCost(2);
		level2.setMoneyCost(5);
		levelController.insertLevel(level2);
		
		logger.info("Lucrezia buys building.");
		userController.buyBuilding("Lucrezia", "Torre");

		logger.info("The users have moneys and gems.");
		logger.info("Building and Special Building are created.");
		logger.info("Experience done. If you want you can continue with Postman.");;
	}

}


