package com.example.clicker.controllers;

import com.example.clicker.entities.Building;
import com.example.clicker.services.BuildingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/building")
public class BuildingController {

    private static final Logger logger = LogManager.getLogger(BuildingController.class);


    @Autowired
    BuildingService buildingService;

    /***************************
     * FUNZIONI CONTROLLER
     *******************************************/

    /***************************
     * Request GET
     *******************************************/

    @GetMapping("/")
    public Iterable<Building> getAllBuildings() throws Exception{
        logger.info("Retrieving all buildings.");
        return buildingService.listBuildings();
    }

    @GetMapping("/building/{name}")
    public Building getBuilding(@PathVariable("name")String name) throws Exception{
        logger.info("Retrieving building's features: " + name);
        return buildingService.getBuilding(name);
    }

    /***************************
     * Request POST
     *******************************************/

    @PostMapping("/insert/")
    public String insertBuilding(@RequestBody Building building) throws Exception{
        logger.info("Inserting new building.");
        buildingService.insertBuilding(building);
        return "New building inserted.";
    }

    /***************************
     * Request PUT
     *******************************************/

    @PutMapping("/updateBuilding")
    public String updateBuilding(@RequestBody Building building) throws Exception{
        logger.info("Changing building values: "+ building.getId());
        logger.info(building.toString());
        buildingService.updateBuilding(building);
        return "Building updated "+ building.getId();
    }

    /***************************
     * Request DELETE
     *******************************************/

    @DeleteMapping("/delete/{buildingName}")
    public String deleteBuilding(@PathVariable("buildingName") String buildingName) throws Exception {
        logger.info("Deleting building: "+ buildingName);
        buildingService.deleteBuilding(buildingName);
        return buildingName;
    }

    @DeleteMapping("/delete")
    public String deleteAll() throws Exception {
        logger.info("Deleting all.");
        buildingService.deleteAll();
        return "Delete done";
    }

}
