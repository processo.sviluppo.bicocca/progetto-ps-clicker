package com.example.clicker.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.clicker.entities.Level;
import com.example.clicker.services.LevelService;

@RestController
@RequestMapping(value = "/level")
public class LevelController {
	
	private static final Logger logger = LogManager.getLogger(LevelController.class);

	
	@Autowired
	LevelService levelService;
	
	/***************************
	 * FUNZIONI CONTROLLER
	 *******************************************/
	
	/***************************
	 * Request GET
	 *******************************************/
	
	@GetMapping("/")
	public Iterable<Level> getAllLevels() throws Exception{
		logger.info("Retrieving all levels.");
		return levelService.allLevels();
	}
	
	@GetMapping("/lvl/{n_level}")
	public Level getLevel(@PathVariable("n_level")Integer n_level) throws Exception{
		logger.info("Retrieving level's features: " + n_level);
		return levelService.getLevel(n_level);
	}

	/***************************
	 * Request POST
	 *******************************************/
	
	@PostMapping("/insert/")
	public String insertLevel(@RequestBody Level level) throws Exception{
		logger.info("Inserting new level.");
		levelService.insertLevel(level);
		return "New level inserted.";
	}
	
	/***************************
	 * Request PUT
	 *******************************************/
	
	@PutMapping("/changeLevel")
	public String changeLevel(@RequestBody Level level) throws Exception{
		logger.info("Changing level's values: "+ level.getId());
		logger.info(level.toString());
		levelService.changeLevel(level);
		return "Level updated "+ level.getId();
	}

	/***************************
	 * Request DELETE
	 *******************************************/

	@DeleteMapping("/delete/{levelId}")
	public Integer deleteBuilding(@PathVariable("levelId") Integer levelId) throws Exception {
		logger.info("Deleting level: "+ levelId);
		levelService.deleteLevel(levelId);
		return levelId;
	}

	@DeleteMapping("/delete")
	public String deleteAll() throws Exception {
		logger.info("Deleting all.");
		levelService.deleteAll();
		return "Delete done";
	}

}
