package com.example.clicker.controllers;

import com.example.clicker.entities.SpecialBuilding;
import com.example.clicker.services.SpecialBuildingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/specialBuilding")
public class SpecialBuildingController {

    private static final Logger logger = LogManager.getLogger(SpecialBuildingController.class);


    @Autowired
    SpecialBuildingService specialBuildingService;

    /***************************
     * FUNZIONI CONTROLLER
     *******************************************/

    /***************************
     * Request GET
     *******************************************/

    @GetMapping("/")
    public Iterable<SpecialBuilding> getAllBuildings() throws Exception {
        logger.info("Retrieving all special buildings.");
        return specialBuildingService.listBuildings();
    }

    @GetMapping("/building/{name}")
    public SpecialBuilding getBuilding(@PathVariable("name") String name) throws Exception {
        logger.info("Retrieving special building's features: " + name);
        return specialBuildingService.getBuilding(name);
    }

    /***************************
     * Request POST
     *******************************************/

    @PostMapping("/insert/")
    public String insertBuilding(@RequestBody SpecialBuilding building) throws Exception {
        logger.info("Inserting new special building.");
        specialBuildingService.insertBuilding(building);
        return "New special building inserted.";
    }

    /***************************
     * Request PUT
     *******************************************/

    @PutMapping("/updateBuilding")
    public String updateBuilding(@RequestBody SpecialBuilding building) throws Exception {
        logger.info("Changing special building's values: " + building.getId());
        logger.info(building.toString());
        specialBuildingService.updateBuilding(building);
        return "Special building updated " + building.getId();
    }

    /***************************
     * Request DELETE
     *******************************************/

    @DeleteMapping("/delete/{buildingName}")
    public String deleteBuilding(@PathVariable("buildingName") String buildingName) throws Exception {
        logger.info("Deleting special building: "+ buildingName);
        specialBuildingService.deleteBuilding(buildingName);
        return buildingName;
    }

    @DeleteMapping("/delete")
    public String deleteAll() throws Exception {
        logger.info("Deleting all.");
        specialBuildingService.deleteAll();
        return "Delete done";
    }

}
