package com.example.clicker.controllers;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.clicker.entities.User;
import com.example.clicker.services.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController {
	
	private static final Logger logger = LogManager.getLogger(UserController.class);

	@Autowired
	UserService userService;
	
	/***************************
	 * FUNZIONI CONTROLLER
	 *******************************************/
	
	/***************************
	 * Request GET
	 *******************************************/
	
	@GetMapping("/")
	public Iterable<User> getUser() throws Exception{
		logger.info("Retrieving the users list.");
		return userService.getUsers();
	}
	
	@GetMapping("/getMoney/{username}")
	public Integer getMoney(@PathVariable("username") String username) throws Exception{
		logger.info("Retrieving user money: "+ username);
		return userService.getMoney(username);
	}
	
	@GetMapping("/getGems/{username}")
	public Integer getGems(@PathVariable("username") String username) throws Exception{
		logger.info("Retrieving user gems: "+ username);
		return userService.getGems(username);
	}
	
	@GetMapping("/whoBF/{username}")
	public String whoBF(@PathVariable("username") String username) throws Exception{
		logger.info("Retrieving best friend's name of "+ username);
		return userService.whoBF(username);
	}
	
	/***************************
	 * Request POST
	 *******************************************/
	
	@PostMapping("/signUp/{username}")
	public String signUp(@PathVariable("username") String username) throws Exception {
		logger.info("Saving the username: "+ username);
		userService.signUp(username);
		return username;
	}
	
	@PostMapping("/{username}/bestFriend")
	public String becomeBF(@PathVariable("username")String username,@RequestParam(name = "usernameBF") String usernameBF) throws Exception{
		logger.info("User "+usernameBF+" and user " + username + " are now best friends.");
		 userService.becomeBF(username, usernameBF);
		 return usernameBF+" bf of "+username;
	}
	
	@PostMapping("donateMoney/{username}")
	public String donateMoney(@PathVariable("username") String username,@RequestParam(name = "usernameBF") String usernameBF) throws Exception{
		logger.info(username+" donates money to "+ usernameBF);
		userService.donateMoney(username, usernameBF);
		return username+" donates money to "+ usernameBF;
		
	}
	
	@PostMapping("donateGems/{username}")
	public String donateGems(@PathVariable("username") String username,@RequestParam(name = "usernameBF") String usernameBF) throws Exception{
		logger.info(username+" donates gems to "+ usernameBF);
		userService.donateGems(username, usernameBF);
		return username+" donates gems to "+ usernameBF;
		
	}
	
	
	/***************************
	 * Request PUT
	 *******************************************/
		
	@PutMapping("/money/{username}")
    public Integer generateMoney(@PathVariable("username") String username) throws Exception {
		logger.info("Adding money to user: "+ username);
        return userService.generateMoney(username);
    }
	
	@PutMapping("/gems/{username}")
    public Integer generateGems(@PathVariable("username") String username) throws Exception {
		logger.info("Adding gems to user: "+ username);
        return userService.generateGems(username);
    }

    @PutMapping("/buyBuilding/{username}")
	public String buyBuilding(@PathVariable("username") String username, @RequestParam("buildingName") String buildingName) throws Exception {
		logger.info(username + " buys building " + buildingName);
		userService.buyBuilding(username, buildingName);
		return username + " buys building " + buildingName;
	}

	@PutMapping("/buySpecialBuilding/{username}")
	public String buySpecialBuilding(@PathVariable("username") String username, @RequestParam("specialBuildingName") String buildingName) throws Exception {
		logger.info(username + " buys special building " + buildingName);
		userService.buySpecialBuilding(username, buildingName);
		return username + " buys special building " + buildingName;
	}

	@PutMapping("/upgradeBuilding/{username}")
	public String upgradeBuilding(@PathVariable("username") String username, @RequestParam("buildingName") String buildingName) throws Exception {
		logger.info(username + " upgrades building " + buildingName);
		userService.upgradeBuilding(username, buildingName);
		return username + " upgrades building " + buildingName;
	}

	@PutMapping("/upgradeSpecialBuilding/{username}")
	public String upgradeSpecialBuilding(@PathVariable("username") String username, @RequestParam("specialBuildingName") String buildingName) throws Exception {
		logger.info(username + " upgrades special building " + buildingName);
		userService.upgradeSpecialBuilding(username, buildingName);
		return username + " upgrades special building " + buildingName;
	}
	
	/***************************
	 * Request DELETE
	 *******************************************/

	@DeleteMapping("/delete/{username}")
	public String deleteUsername(@PathVariable("username") String username) throws Exception {
		logger.info("Deleting username: "+ username);
		userService.deleteUser(username);
		return username;
	}
	
	@DeleteMapping("/delete")
	public String deleteAll() throws Exception {
		logger.info("Deleting all.");
		userService.deleteAll();
		return "Delete done";
	}
}
