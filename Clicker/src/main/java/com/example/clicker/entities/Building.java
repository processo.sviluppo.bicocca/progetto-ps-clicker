package com.example.clicker.entities;

import java.util.Objects;

import javax.persistence.*;


@Entity(name = "building")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Building {

    @Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id_building")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "floors")
    private Integer floors;
    @Column(name = "area")
    private Integer area;
    @Column(name = "people")
    private Integer people;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFloors() {
        return floors;
    }

    public void setFloors(Integer floors) {
        this.floors = floors;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public Integer getPeople() {
        return people;
    }

    public void setPeople(Integer people) {
        this.people = people;
    }

    @Override
    public String toString() {
        return "Building{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", floors=" + floors +
                ", area=" + area +
                ", people=" + people +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Building building = (Building) o;
        return Objects.equals(id, building.id) && Objects.equals(name, building.name) && Objects.equals(description, building.description) && Objects.equals(floors, building.floors) && Objects.equals(area, building.area) && Objects.equals(people, building.people);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, floors, area, people);
    }
}
