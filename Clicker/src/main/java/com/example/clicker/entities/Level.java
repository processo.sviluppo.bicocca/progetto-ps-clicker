package com.example.clicker.entities;

import javax.persistence.*;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
@RequiredArgsConstructor 

@Entity(name = "level")
public class Level {
	
    @Id
    @Column(name = "n_level")
    private Integer id;
    @NonNull
    @Column(name = "money_cost")
    private Integer moneyCost;
    @NonNull
    @Column(name = "gems_cost")
    private Integer gemsCost;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMoneyCost() {
        return moneyCost;
    }

    public void setMoneyCost(Integer moneyCost) {
        this.moneyCost = moneyCost;
    }

    public Integer getGemsCost() {
        return gemsCost;
    }

    public void setGemsCost(Integer gemsCost) {
        this.gemsCost = gemsCost;
    }

    @Override
    public String toString() {
        return "Level{" +
                "id=" + id +
                ", moneyCost=" + moneyCost +
                ", gemsCost=" + gemsCost +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Level level = (Level) o;
        return Objects.equals(id, level.id) && Objects.equals(moneyCost, level.moneyCost) && Objects.equals(gemsCost, level.gemsCost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, moneyCost, gemsCost);
    }
}
