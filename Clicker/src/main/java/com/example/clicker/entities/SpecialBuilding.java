package com.example.clicker.entities;



import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Objects;

@Entity(name = "special_building")
public class SpecialBuilding extends Building {
	
    @Column(name = "congress_rooms")
    private Integer congressRooms;

    public Integer getCongressRooms() {
        return congressRooms;
    }

    public void setCongressRooms(Integer congressRooms) {
        this.congressRooms = congressRooms;
    }

    @Override
    public String toString() {
        return "SpecialBuilding{" +
                "congressRooms=" + congressRooms +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SpecialBuilding that = (SpecialBuilding) o;
        return Objects.equals(congressRooms, that.congressRooms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), congressRooms);
    }
}
