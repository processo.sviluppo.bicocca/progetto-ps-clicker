package com.example.clicker.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity(name = "user")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    @Column(name = "username")
    private String username;
    @Column(name = "money")
    private Integer money;
    @Column(name = "gems")
    private Integer gems;
    
    @OneToOne
    @JsonIgnore   
    @JoinColumn(name="id_best_friend")
    private User bestFriend;

    @OneToMany(mappedBy = "key.user",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<UserBuilding> buildings = new ArrayList<UserBuilding>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getGems() {
        return gems;
    }

    public void setGems(Integer gems) {
        this.gems = gems;
    }

    public User getBestFriend() {
        return bestFriend;
    }

    public void setBestFriend(User bestFriend) {
        this.bestFriend = bestFriend;
    }

    public List<UserBuilding> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<UserBuilding> buildings) {
        this.buildings = buildings;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", money=" + money +
                ", gems=" + gems +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(username, user.username) && Objects.equals(money, user.money) && Objects.equals(gems, user.gems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, money, gems);
    }
}
