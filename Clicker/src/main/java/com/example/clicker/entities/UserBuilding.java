package com.example.clicker.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import java.util.Objects;

@Entity(name = "USER_BUILDING")
public class UserBuilding implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private UserBuildingKey key;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UserBuildingKey getKey() {
        return key;
    }

    public void setKey(UserBuildingKey key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "UserBuilding{" +
                "key=" + key +
                '}';
    }

    public static class UserBuildingKey implements Serializable {


		private static final long serialVersionUID = 1L;

		@ManyToOne
        @JoinColumn(name = "user_id", referencedColumnName = "id")
        @JsonBackReference
        private User user;

        @ManyToOne
        @JoinColumn(name = "buildings_id_building", referencedColumnName = "id_building")
        @JsonIgnore
        private Building building;

        @ManyToOne
        @JoinColumn(name = "n_level", referencedColumnName = "n_level" )
        @JsonIgnore
        private Level level;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Building getBuilding() {
            return building;
        }

        public void setBuilding(Building building) {
            this.building = building;
        }

        public Level getLevel() {
            return level;
        }

        public void setLevel(Level level) {
            this.level = level;
        }

        @Override
        public String toString() {
            return "UserBuildingKey";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            UserBuildingKey that = (UserBuildingKey) o;
            return Objects.equals(user.getUsername(), that.user.getUsername()) && Objects.equals(building.getName(), that.building.getName()) && Objects.equals(level.getId(), that.level.getId());
        }

        @Override
        public int hashCode() {
            return Objects.hash(user.getUsername(), building.getName(), level.getId());
        }
    }
}
