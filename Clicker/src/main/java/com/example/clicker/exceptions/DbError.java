package com.example.clicker.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class DbError  extends RuntimeException {
	 
    private static final long serialVersionUID = 1L;

   public DbError(String message) {
       super(message);
   }

   public DbError() {
       super("Errore di connessione con il Database.");
   }

}
