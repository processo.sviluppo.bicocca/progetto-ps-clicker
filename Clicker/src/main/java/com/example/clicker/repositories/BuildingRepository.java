package com.example.clicker.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.clicker.entities.Building;

public interface BuildingRepository extends CrudRepository<Building, Integer> {
	Building findByName(String name);
	}
