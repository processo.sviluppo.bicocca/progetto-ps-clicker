package com.example.clicker.repositories;

import com.example.clicker.entities.Level;
import org.springframework.data.repository.CrudRepository;

public interface LevelRepository extends CrudRepository<Level, Integer> {

}
