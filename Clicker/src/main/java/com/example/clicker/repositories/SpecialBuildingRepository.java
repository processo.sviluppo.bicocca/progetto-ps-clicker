package com.example.clicker.repositories;

import com.example.clicker.entities.SpecialBuilding;
import org.springframework.data.repository.CrudRepository;

public interface SpecialBuildingRepository extends CrudRepository<SpecialBuilding, Integer> {
    SpecialBuilding findByName(String name);
}
