package com.example.clicker.repositories;

import com.example.clicker.entities.UserBuilding;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserBuildingRepository extends CrudRepository<UserBuilding, UserBuilding.UserBuildingKey> {
    public List<UserBuilding> findAll();
}
