package com.example.clicker.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.clicker.entities.User;


@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
	User findByUsername(String username);
}
