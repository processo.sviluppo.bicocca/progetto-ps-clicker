package com.example.clicker.services;

import com.example.clicker.entities.Building;

public interface BuildingService {

    public Iterable<Building> listBuildings();

    public Building getBuilding(String name);

    public void insertBuilding(Building building) throws Exception;

    public void updateBuilding(Building building) throws Exception;

    public void deleteBuilding(String buildingName) throws Exception;

    public void deleteAll() throws Exception;

}
