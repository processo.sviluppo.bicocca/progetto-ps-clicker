package com.example.clicker.services.Impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.clicker.entities.Building;
import com.example.clicker.exceptions.DbError;
import com.example.clicker.exceptions.EntityNotFoundException;
import com.example.clicker.repositories.BuildingRepository;
import com.example.clicker.services.BuildingService;

import java.util.Optional;

@Service
public class BuildingServiceImpl implements BuildingService {
	
	@Autowired
	BuildingRepository buildingRepository;
	
	@Override
	public Iterable<Building> listBuildings(){
		try {
			return buildingRepository.findAll();
		} catch (Exception e) {
			throw new DbError();
		}
	}
	
	@Override
	public Building getBuilding(String name) {
		try {
			return buildingRepository.findByName(name);
	    } catch (Exception e) {
	    	throw new EntityNotFoundException("This building is not in the database.");
	    }
	}

	@Override
	public void insertBuilding(Building building) throws Exception {
		try {
			Building buildingTmp = new Building();
			buildingTmp.setName(building.getName());
			buildingTmp.setDescription(building.getDescription());
			buildingTmp.setFloors(building.getFloors());
			buildingTmp.setArea(building.getArea());
			buildingTmp.setPeople(building.getPeople());
			buildingRepository.save(buildingTmp);
		}catch (Exception e) {
			throw new DbError();
		}
	}

	@Override
	public void updateBuilding(Building building) throws Exception{
		try {
			Optional<Building> buildingOpt = buildingRepository.findById(building.getId());
			if(buildingOpt.isPresent()) {
				Building buildingTmp = new Building();
				buildingTmp.setName(building.getName());
				buildingTmp.setDescription(building.getDescription());
				buildingTmp.setFloors(building.getFloors());
				buildingTmp.setArea(building.getArea());
				buildingTmp.setPeople(building.getPeople());
				buildingRepository.save(building);
			}
			else throw new Exception("Building not existent.");
		}catch (Exception e) {
			throw new DbError();
		}
	}

	@Override
	public void deleteBuilding(String buildingName) throws Exception {
		try {
			Building building = buildingRepository.findByName(buildingName);
			buildingRepository.delete(building);
		} catch (Exception e) {
			throw new EntityNotFoundException("The building with name " + buildingName + " is not in the database.");
		}
	}

	@Override
	public void deleteAll() throws Exception{
		try {
			buildingRepository.deleteAll();
		} catch (Exception e) {
			throw new DbError();
		}
	}

}
