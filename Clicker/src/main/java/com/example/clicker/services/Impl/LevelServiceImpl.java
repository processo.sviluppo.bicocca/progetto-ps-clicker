package com.example.clicker.services.Impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.clicker.repositories.LevelRepository;
import com.example.clicker.services.LevelService;
import com.example.clicker.entities.Level;
import com.example.clicker.exceptions.DbError;
import com.example.clicker.exceptions.EntityNotFoundException;

@Service
public class LevelServiceImpl implements LevelService{
	
	@Autowired
	LevelRepository levelRepository;
	
	@Override
	public Iterable<Level> allLevels() throws Exception {
		try {
			return levelRepository.findAll();
		}catch (Exception e) {
			throw new DbError();
		}

	}
	
	@Override
	public Level getLevel(Integer n_level) throws Exception {
		try {
			Optional <Level> levelOpt = levelRepository.findById(n_level);
			Level level = new Level();
			level.setId(n_level);
			level.setGemsCost(levelOpt.get().getGemsCost());
			level.setMoneyCost(levelOpt.get().getMoneyCost());
			return level;
        } catch (Exception e) {
        	throw new EntityNotFoundException("Livello non presente.");
        }
		
	}
	
	@Override
	public void insertLevel(Level level) throws Exception {
		try {
			Level levelTmp = new Level();
			levelTmp.setGemsCost(level.getGemsCost());
			levelTmp.setMoneyCost(level.getMoneyCost());
			levelTmp.setId(level.getId());
			levelRepository.save(levelTmp);
		}catch (Exception e) {
			throw new DbError();
		}
	}
	
	@Override
	public void changeLevel(Level level) throws Exception{
		try {
			Optional<Level> levelOpt = levelRepository.findById(level.getId());
			if(levelOpt.isPresent()) {
				Level levelTmp = new Level();
				levelTmp.setId(level.getId());
				levelTmp.setGemsCost(level.getGemsCost());
				levelTmp.setMoneyCost(level.getMoneyCost());
				levelRepository.save(level);
			} 
			else throw new Exception("Level not existent.");
		}catch (Exception e) {
			throw new DbError();
		}
	}

	@Override
	public void deleteLevel(Integer levelId) throws Exception {
		try {
			Level level = levelRepository.findById(levelId).get();
			levelRepository.delete(level);
		} catch (Exception e) {
			throw new EntityNotFoundException("The level with id " + levelId + " is not in the database.");
		}
	}

	@Override
	public void deleteAll() throws Exception{
		try {
			levelRepository.deleteAll();
		} catch (Exception e) {
			throw new DbError();
		}
	}
	
}
