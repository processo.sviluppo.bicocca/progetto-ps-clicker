package com.example.clicker.services.Impl;


import com.example.clicker.entities.SpecialBuilding;
import com.example.clicker.repositories.SpecialBuildingRepository;
import com.example.clicker.services.SpecialBuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.clicker.exceptions.DbError;
import com.example.clicker.exceptions.EntityNotFoundException;


import java.util.Optional;

@Service
public class SpecialBuildingServiceImpl implements SpecialBuildingService {

    @Autowired
    SpecialBuildingRepository specialBuildingRepository;

    @Override
    public Iterable<SpecialBuilding> listBuildings(){
        try {
            return specialBuildingRepository.findAll();
        } catch (Exception e) {
            throw new DbError();
        }
    }

    @Override
    public SpecialBuilding getBuilding(String name) {
        try {
            return specialBuildingRepository.findByName(name);
        } catch (Exception e) {
            throw new EntityNotFoundException("This building is not in the database.");
        }
    }

    @Override
    public void insertBuilding(SpecialBuilding building) throws Exception {
        try {
            SpecialBuilding buildingTmp = new SpecialBuilding();
            buildingTmp.setName(building.getName());
            buildingTmp.setDescription(building.getDescription());
            buildingTmp.setFloors(building.getFloors());
            buildingTmp.setArea(building.getArea());
            buildingTmp.setPeople(building.getPeople());
            buildingTmp.setCongressRooms(building.getCongressRooms());
            specialBuildingRepository.save(buildingTmp);
        }catch (Exception e) {
            throw new DbError();
        }
    }

    @Override
    public void updateBuilding(SpecialBuilding building) throws Exception{
        try {
            Optional<SpecialBuilding> buildingOpt = specialBuildingRepository.findById(building.getId());
            if(buildingOpt.isPresent()) {
                SpecialBuilding buildingTmp = new SpecialBuilding();
                buildingTmp.setName(building.getName());
                buildingTmp.setDescription(building.getDescription());
                buildingTmp.setFloors(building.getFloors());
                buildingTmp.setArea(building.getArea());
                buildingTmp.setPeople(building.getPeople());
                buildingTmp.setCongressRooms(building.getCongressRooms());
                specialBuildingRepository.save(building);
            }
            else throw new Exception("Building not existent.");
        }catch (Exception e) {
            throw new DbError();
        }
    }

    @Override
    public void deleteBuilding(String buildingName) throws Exception {
        try {
            SpecialBuilding building = specialBuildingRepository.findByName(buildingName);
            specialBuildingRepository.delete(building);
        } catch (Exception e) {
            throw new EntityNotFoundException("The building with name " + buildingName + " is not in the database.");
        }
    }

    @Override
    public void deleteAll() throws Exception{
        try {
            specialBuildingRepository.deleteAll();
        } catch (Exception e) {
            throw new DbError();
        }
    }

}
