package com.example.clicker.services.Impl;

import com.example.clicker.entities.*;
import com.example.clicker.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.clicker.exceptions.DbError;
import com.example.clicker.exceptions.EntityNotFoundException;
import com.example.clicker.services.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    BuildingRepository buildingRepository;
    @Autowired
    SpecialBuildingRepository specialBuildingRepository;
    @Autowired
    LevelRepository levelRepository;
    @Autowired
    UserBuildingRepository userBuildingRepository;

    @Override
    public Iterable<User> getUsers() throws Exception {
        try {
            return userRepository.findAll();
        } catch (Exception e) {
            throw new DbError();
        }
    }

    @Override
    public void signUp(String username) throws Exception {
        try {
            User user = new User();
            user.setUsername(username);
            user.setGems(0);
            user.setMoney(0);
            user.setBuildings(null);
            user.setBestFriend(null);
            userRepository.save(user);
        } catch (Exception e) {
            throw new DbError();
        }
    }

    @Override
    public void becomeBF(String username, String usernameBF) throws Exception {
        try {
            User user = userRepository.findByUsername(username);
            User userBF = userRepository.findByUsername(usernameBF);
            user.setBestFriend(userBF);
            userBF.setBestFriend(user);
            userRepository.save(user);
            userRepository.save(userBF);
        } catch (Exception e) {
            throw new EntityNotFoundException("One of the usernames is not in the database.");
        }
    }

    @Override
    public String whoBF(String username) throws Exception {
        try {
            User user = userRepository.findByUsername(username);
            if (user.getBestFriend() != null)
                return user.getBestFriend().getUsername();
            else
                return "This user doesn't have best friends.";
        } catch (Exception e) {
            throw new DbError();
        }
    }

    @Override
    public Integer generateMoney(String username) throws Exception {
        try {
            User user = userRepository.findByUsername(username);
            user.setMoney(user.getMoney() + 10);
            userRepository.save(user);
            return user.getMoney();
        } catch (Exception e) {
            throw new EntityNotFoundException("The username " + username + " is not in the database.");
        }
    }

    @Override
    public Integer generateGems(String username) throws Exception {
        try {
            User user = userRepository.findByUsername(username);
            user.setGems(user.getGems() + 5);
            userRepository.save(user);
            return user.getGems();
        } catch (Exception e) {
            throw new EntityNotFoundException("The username " + username + " is not in the database.");
        }
    }

    @Override
    public Integer getGems(String username) throws Exception {
        try {
            User user = userRepository.findByUsername(username);
            return user.getGems();
        } catch (Exception e) {
            throw new EntityNotFoundException("The username " + username + " is not in the database.");
        }
    }

    @Override
    public Integer getMoney(String username) throws Exception {
        try {
            User user = userRepository.findByUsername(username);
            return user.getMoney();
        } catch (Exception e) {
            throw new EntityNotFoundException("The username " + username + " is not in the database.");
        }
    }

    @Override
    public void donateMoney(String username, String usernameBF) throws Exception {

        User user = userRepository.findByUsername(username);
        User userBF = userRepository.findByUsername(usernameBF);
        if (user == null || userBF == null) throw new Exception("One of the usernames is not in the database.");

        if (userBF.getBestFriend() != null && userBF.getBestFriend().getUsername().equals(username)) {
            if (user.getMoney() != null && user.getMoney() >= 10) {
                user.setMoney(user.getMoney() - 10);
                userBF.setMoney(userBF.getMoney() + 10);
            } else throw new Exception("The user hasn't enough money.");
        } else throw new Exception("These useers are not best friend.");

        userRepository.save(user);
        userRepository.save(userBF);
    }

    @Override
    public void donateGems(String username, String usernameBF) throws Exception {

        User user = userRepository.findByUsername(username);
        User userBF = userRepository.findByUsername(usernameBF);
        if (user == null || userBF == null) throw new Exception("One of the usernames is not in the database.");

        if (userBF.getBestFriend() != null && userBF.getBestFriend().getUsername().equals(username)) {
            if (user.getGems() != null && user.getGems() >= 5) {
                user.setGems(user.getGems() - 5);
                userBF.setGems(userBF.getGems() + 5);
            } else throw new Exception("The user hasn't enough gems.");
        } else throw new Exception("These users are not best friend.");

        userRepository.save(user);
        userRepository.save(userBF);
    }

    @Override
    public void buyBuilding(String username, String buildingName) throws Exception {
        User user = userRepository.findByUsername(username);
        Building building = buildingRepository.findByName(buildingName);

        if (user == null || building == null) throw new Exception("User or building not existent.");
        for (UserBuilding userBuilding : user.getBuildings()) {
            if (userBuilding.getKey().getBuilding().getName().equals(building.getName()))
                throw new Exception("This user already ownes this building.");
        }
        if (user.getMoney() < 10) throw new Exception("The user hasn't enough money.");

        UserBuilding userBuilding = new UserBuilding();
        userBuilding.setKey(new UserBuilding.UserBuildingKey());
        userBuilding.getKey().setUser(user);
        userBuilding.getKey().setBuilding(building);
        userBuilding.getKey().setLevel(levelRepository.findById(1).get());
        user.getBuildings().add(userBuilding);
        user.setMoney(user.getMoney() - 10);

        userRepository.save(user);
    }

    @Override
    public void buySpecialBuilding(String username, String buildingName) throws Exception {
        User user = userRepository.findByUsername(username);
        SpecialBuilding building = specialBuildingRepository.findByName(buildingName);

        if (user == null || building == null) throw new Exception("User or building not existent.");
        for (UserBuilding userBuilding : user.getBuildings()) {
            if (userBuilding.getKey().getBuilding().getName().equals(building.getName()))
                throw new Exception("This user already ownes this building.");
        }
        if (user.getGems() < 5) throw new Exception("The user hasn't enough gems.");

        UserBuilding userBuilding = new UserBuilding();
        userBuilding.setKey(new UserBuilding.UserBuildingKey());
        userBuilding.getKey().setUser(user);
        userBuilding.getKey().setBuilding(building);
        userBuilding.getKey().setLevel(levelRepository.findById(1).get());
        user.getBuildings().add(userBuilding);
        user.setGems(user.getGems() - 5);

        userRepository.save(user);
    }

    @Override
    public void upgradeBuilding(String username, String buildingName) throws Exception {
        User user = userRepository.findByUsername(username);
        Building building = buildingRepository.findByName(buildingName);

        if (user == null || building == null) throw new Exception("User or building not existent.");

        List<UserBuilding> userBuildings = user.getBuildings();
        for (UserBuilding userBuilding : userBuildings) {
            if (userBuilding.getKey().getBuilding().getName().equals(buildingName)) {
                Optional<Level> newLevelOpt = levelRepository.findById(userBuilding.getKey().getLevel().getId() + 1);
                Level newLevel;
                if (newLevelOpt.isPresent()) {
                    newLevel = newLevelOpt.get();
                } else {
                    throw new Exception("This building is already at the maximum level.");
                }
                if (user.getMoney() < newLevel.getMoneyCost()) throw new Exception("The user hasn't enough gems.");

                user.getBuildings().remove(userBuilding);
                userRepository.save(user);
                userBuildingRepository.delete(userBuilding);

                user.setMoney(user.getMoney() - newLevel.getMoneyCost());
                UserBuilding ub = new UserBuilding();
                ub.setKey(new UserBuilding.UserBuildingKey());
                ub.getKey().setLevel(newLevel);
                ub.getKey().setUser(user);
                ub.getKey().setBuilding(building);
                user.getBuildings().add(ub);
                userRepository.save(user);
                return;
            }
        }

        throw new Exception("The user doesn't own this building.");
    }

    @Override
    public void upgradeSpecialBuilding(String username, String buildingName) throws Exception {
        User user = userRepository.findByUsername(username);
        SpecialBuilding building = specialBuildingRepository.findByName(buildingName);

        if (user == null || building == null) throw new Exception("User or building not existent.");

        for (UserBuilding userBuilding : user.getBuildings()) {
            if (userBuilding.getKey().getBuilding().getName().equals(buildingName)) {
                Optional<Level> newLevelOpt = levelRepository.findById(userBuilding.getKey().getLevel().getId() + 1);
                Level newLevel;
                if (newLevelOpt.isPresent()) {
                    newLevel = newLevelOpt.get();
                } else {
                    throw new Exception("This building is already at the maximum level.");
                }
                if (user.getMoney() < newLevel.getMoneyCost()) throw new Exception("The user hasn't enough gems.");

                user.getBuildings().remove(userBuilding);
                userRepository.save(user);
                userBuildingRepository.delete(userBuilding);

                user.setMoney(user.getGems() - newLevel.getGemsCost());
                UserBuilding ub = new UserBuilding();
                ub.setKey(new UserBuilding.UserBuildingKey());
                ub.getKey().setLevel(newLevel);
                ub.getKey().setUser(user);
                ub.getKey().setBuilding(building);
                user.getBuildings().add(ub);
                userRepository.save(user);
                return;
            }
        }
        throw new Exception("The user doesn't own this building.");
    }

    @Override
    public void deleteUser(String username) throws Exception {
        try {
            User user = userRepository.findByUsername(username);
            userRepository.delete(user);
        } catch (Exception e) {
            throw new EntityNotFoundException("The username " + username + " is not in the database.");
        }
    }

    @Override
    public void deleteAll() throws Exception {
        try {
            userRepository.deleteAll();
        } catch (Exception e) {
            throw new DbError();
        }
    }

}
