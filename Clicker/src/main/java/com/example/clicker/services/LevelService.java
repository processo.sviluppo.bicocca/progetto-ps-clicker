package com.example.clicker.services;

import com.example.clicker.entities.Level;

public interface LevelService  {
	
	public Level getLevel(Integer n_level) throws Exception;
	public Iterable<Level> allLevels() throws Exception;
	public void changeLevel(Level level) throws Exception;
	public void insertLevel(Level level) throws Exception;
	public void deleteLevel(Integer levelId) throws Exception;
	public void deleteAll() throws Exception;
}
