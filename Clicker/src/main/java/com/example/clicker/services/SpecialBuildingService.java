package com.example.clicker.services;

import com.example.clicker.entities.SpecialBuilding;

public interface SpecialBuildingService {

    public Iterable<SpecialBuilding> listBuildings();

    public SpecialBuilding getBuilding(String name);

    public void insertBuilding(SpecialBuilding building) throws Exception;

    public void updateBuilding(SpecialBuilding building) throws Exception;

    public void deleteBuilding(String buildingName) throws Exception;

    public void deleteAll() throws Exception;

}
