package com.example.clicker.services;

import com.example.clicker.entities.User;

public interface UserService {

	public Iterable<User> getUsers() throws Exception;
	public void signUp(String username) throws Exception;
	public Integer generateMoney(String username)throws Exception;
	public Integer generateGems(String username) throws Exception;
	public void donateMoney(String username, String usernameBF) throws Exception;
	public void donateGems(String username, String usernameBF) throws Exception;
	public Integer getMoney(String username) throws Exception;
	public Integer getGems(String username) throws Exception;
	public void deleteUser(String username) throws Exception;
	public void deleteAll() throws Exception;
	public void becomeBF(String username, String usernameBF) throws Exception;
	public String whoBF(String username) throws Exception;
	public void buyBuilding(String username, String buildingName) throws Exception;
	public void buySpecialBuilding(String username, String buildingName) throws Exception;
	public void upgradeBuilding(String username, String buildingName) throws Exception;
	public void upgradeSpecialBuilding(String username, String buildingName) throws Exception;

}
