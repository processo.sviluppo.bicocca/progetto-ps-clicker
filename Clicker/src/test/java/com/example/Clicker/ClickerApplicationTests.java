//package com.example.Clicker;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import com.example.clicker.controllers.UserController;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//class ClickerApplicationTests {
//
//	private static final Logger logger = LogManager.getLogger(UserController.class);
//
//	UserController userController;
//	
//	@Test
//	void contextLoads() throws Exception {
//		
//		this.userController=new UserController();
//		
//		logger.info("Start Click-game experience.");
//				
//		logger.info("Add the first user Lucrezia.");
//	
//		this.userController.signUp("Lucrezia");
//		
//		logger.info("Add the second user Achille.");
//		
//		userController.signUp("Achille");
//		
//		logger.info("Set Achille as best friend of Lucrezia.");
//		
//		userController.becomeBF("Lucrezia","Achille");
//		
//		logger.info("Lucrezia is clicking 3 times and gains 15 gems.");
//		
//		for(int i=0;i<3;i++)
//			userController.generateGems("Lucrezia");
//		
//		logger.info("Lucrezia is clicking 3 times and gains 30 money.");
//		
//		for(int i=0;i<3;i++)
//			userController.generateMoney("Lucrezia");
//	}
//
//}
