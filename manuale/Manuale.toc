\contentsline {section}{\numberline {1}Overview}{1}{}%
\contentsline {paragraph}{}{1}{}%
\contentsline {paragraph}{}{1}{}%
\contentsline {section}{\numberline {2}Le regole del gioco}{1}{}%
\contentsline {paragraph}{}{1}{}%
\contentsline {section}{\numberline {3}Vincoli dell'assignment}{2}{}%
\contentsline {paragraph}{User}{2}{}%
\contentsline {paragraph}{Building}{2}{}%
\contentsline {paragraph}{Level}{2}{}%
\contentsline {paragraph}{SpecialBuilding}{2}{}%
\contentsline {section}{\numberline {4}La struttura del software}{3}{}%
\contentsline {paragraph}{Clicker}{3}{}%
\contentsline {paragraph}{Controllers}{3}{}%
\contentsline {paragraph}{Etities}{3}{}%
\contentsline {paragraph}{Exceptions}{3}{}%
\contentsline {paragraph}{Repositories}{3}{}%
\contentsline {paragraph}{Services}{3}{}%
\contentsline {paragraph}{Services.Impl}{3}{}%
\contentsline {section}{\numberline {5}Database}{3}{}%
\contentsline {section}{\numberline {6}Cosa abbiamo implementato}{4}{}%
\contentsline {subsection}{\numberline {6.1}Username Controller}{4}{}%
\contentsline {subsection}{\numberline {6.2}Building Controller}{5}{}%
\contentsline {subsection}{\numberline {6.3}Level Controller}{5}{}%
\contentsline {subsection}{\numberline {6.4}Special Building Controller}{5}{}%
\contentsline {section}{\numberline {7}Sequence Diagrams}{6}{}%
\contentsline {subsection}{\numberline {7.1}Add new user}{6}{}%
\contentsline {subsection}{\numberline {7.2}Get Special Building By Name}{6}{}%
\contentsline {section}{\numberline {8}Come si effettua il deploy}{7}{}%
\contentsline {section}{\numberline {9}Come si verifica il funzionamento}{8}{}%
\contentsline {subsection}{\numberline {9.1}Collection Postman}{8}{}%
\contentsline {subsection}{\numberline {9.2}Classe Clicker Experience}{8}{}%
\contentsline {section}{\numberline {10}Limitazioni Database}{11}{}%
